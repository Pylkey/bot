from pprint import pprint

import httplib2
import apiclient.discovery
from oauth2client.service_account import ServiceAccountCredentials

CREDENTIAL_FILE='google-sheets/creds.json' # Файл который нам прислал Google
sheet_id='1XpQi2dx_McLvyAEeDaJLoX-39o1yoKH6pmeo7N94wTA' # Документ с которым работаем

credentials = ServiceAccountCredentials.from_json_keyfile_name(
    CREDENTIAL_FILE,
    ['https://www.googleapis.com/auth/spreadsheets',
    'https://www.googleapis.com/auth/drive']
)
httpAuth = credentials.authorize(httplib2.Http()) # объект для работы с аутентификацией
service = apiclient.discovery.build('sheets', 'v4', http=httpAuth) # получаем/отправляем данные для нашего документа

# Читаем данные
result = service.spreadsheets().values().get(
    spreadsheetId=sheet_id,
    range='A1:E13',
    majorDimension='ROWS'
).execute()
data = result.get('values', [])
pprint(data)

# Пишем данные
result = service.spreadsheets().values().batchUpdate(
    spreadsheetId=sheet_id,
    body={
        'valueInputOption':'RAW',
        'data': [{
            'range':'G6:J10',
            'majorDimension':'ROWS',
            'values':[['testG6', 'testG10'], ['testJ6', 'testJ10']]
            },
            {
            'range':'H7:I10',
            'majorDimension':'ROWS',
            'values':[['testH7', 'testH9'], ['testI7', 'testI9']]
            }]
    }
).execute()


